BIN = bin
SRC = src
INC = include
OBJ = obj
LIB = lib

CFLAGS += -I ${INC} --std=c99 -Wall -Wextra -Werror

BIN_LDFLAGS = -L./${LIB} -lbst

LIB_CFLAGS  = -fPIC
LIB_LDFLAGS = -shared

all: ${BIN}/test ${LIB}/libbst.so

clean:
	rm -rf ${BIN}/ ${LIB}/ ${OBJ}

test: ${BIN}/test
	LD_LIBRARY_PATH=${LIB} ${BIN}/test

.PHONY: all clean test

#${LDFLAGS} needed for people with non stantart lib location
#usage: LDFLAGS="-L/..." make 

${BIN}/test: ${OBJ}/main.o | ${BIN} ${LIB}/libbst.so
	cc $< ${BIN_LDFLAGS} ${LDFLAGS} -o $@

${LIB}/libbst.so: ${OBJ}/bst.o | ${LIB}
	cc $< ${LIB_LDFLAGS} ${LDFLAGS} -o $@

${OBJ}/bst.o: ${SRC}/bst.c | ${OBJ} ${INC}/bst.h
	cc $< -c ${CFLAGS} ${LIB_CFLAGS} -o $@

${OBJ}/main.o: ${SRC}/main.c | ${OBJ} ${INC}/bst.h
	cc $< -c ${CFLAGS} -o $@

${BIN}:
	mkdir -p $@/

${LIB}:
	mkdir -p $@/

${OBJ}:
	mkdir -p $@/

