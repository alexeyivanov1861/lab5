#ifndef __BST__
#define __BST__
#include <stddef.h>

typedef struct tree {
	struct tree *left, *right;
	void *value;
	size_t value_size;
	void *key;
	size_t key_size;
} tree;

tree* search_node(tree *root, void *key, int (*cmp)(void *, void *));
void* search(tree *root, void *key, int (*cmp)(void *, void *));
void insert(tree **root, void *value, size_t value_size, void *key, size_t key_size, int (*cmp)(void *, void *));
int rang(tree *);
tree* minimum(tree *root);
tree* maximum(tree *root);
void ptr_swap(void **, void **);
void* delete(tree *, void *, int (*cmp)(void *, void*), void* (*destructor)(void *, void *));
void* clear(tree **, int (*cmp)(void *, void *), void* (*destructor)(void*, void*));
#endif
