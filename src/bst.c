#include "bst.h"
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

tree* search_node(tree *root, void *key, int (*cmp)(void *, void *)) {
	while (root) {
		int cmp_result = cmp(key, root->key);
		if (cmp_result < 0) {
			root = root->left;
		}
		else if (cmp_result > 0){
			root = root->right;
		}
		else
			return root;
	}
	return NULL;
}

void* search(tree *root, void *key, int (*cmp)(void *, void*)) {
	tree *f = search_node(root, key, cmp);
	if (f)
		return f->value;
	return NULL;
}

void insert(tree **root, void *value, size_t value_size, void *key, size_t key_size, int (*cmp)(void *, void *)) {
	while (*root) {
		int cmp_result = cmp(key, (*root)->key);
		if (cmp_result < 0) {
			root = &(*root)->left;
		} else {
			root = &(*root)->right;
		}
	}
	tree *n = malloc(sizeof(tree));
	n->key_size = key_size;
	n->key = malloc(key_size);
	n->value_size = value_size;
	n->value = malloc(value_size);
	n->left = NULL;
	n->right = NULL;

	memcpy(n->key, key, key_size);
	memcpy(n->value, value, value_size);

	*root = n;
}

int rang(tree *root) {
	int r = -1;
	if (root) {
		++r;
		if (root->left)
			++r;
		if (root->right)
			++r;
	}
	return r;
}

tree* minimum(tree *root) {
	tree *prev;
	do {
		prev = root;
		root = root->left;
	} while (root);
	return prev;
}

tree* maximum(tree *root) {
	tree *prev;
	do {
		prev = root;
		root = root->right;
	} while (root);
	return prev;
}

void ptr_swap(void **p1, void **p2) {
	void *t = *p1;
	*p1 = *p2;
	*p2 = t;
}

void* delete(tree *root, void *key, int (*cmp)(void *, void *), void* (*destructor)(void *, void *)) {
	if (root == NULL)
		return NULL;
	/* search */
	int cmp_result = cmp(key, root->key);
	if (cmp_result < 0)
		root->left = delete(root->left, key, cmp, destructor);
	else if (cmp_result > 0)
		root->right = delete(root->right, key, cmp, destructor);
	/* delete */
	else {
		if (root->left == NULL && root->right == NULL) {
			destructor(root->value, root->key);//
			free(root);
			return NULL;
		}
		else if (root->left != NULL && root->right == NULL) {
			tree *tmp = root->left;
			destructor(root->value, root->key); //
			free(root);
			return tmp;
		}
		else if (root->left == NULL && root->right != NULL) {
			tree *tmp = root->right;
			destructor(root->value, root->key); //
			free(root);
			return tmp;
		}
		else {
			tree *tmp_node = minimum(root->right);
			ptr_swap(&tmp_node->value, &root->value);
			ptr_swap(&tmp_node->key, &root->key);
			root->right = delete(root->right, key, cmp, destructor);
		}
	}
	return root;
}

void* clear(tree **root, int (*cmp)(void *, void*), void* (*destructor)(void *, void *)) {
	while (*root)
		*root = delete(*root, (*root)->key, cmp, destructor);
	return NULL;
}
