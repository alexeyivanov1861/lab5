#include "bst.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <time.h>

int cmp(void *p1, void *p2) {
	return *(int*)p1 - *(int*)p2;
}

void print(tree* root) {
	if (root == NULL)
		return;
	print(root->left);
	printf("%s with key %d\n", (char*)root->value, *(int*)root->key);
	print(root->right);
}

void get_rand_str(char *str, size_t len) {
	unsigned i;
	for (i = 0; i < len; i++)
		*(str + i) = 'A' + (rand() % ('z' - 'A' + 1));
	*(str + i) = '\0';
}

tree* getdata() { //insert example here
	srand(time(NULL));
	tree *root = NULL;
	char buf[32];
	int key;
	int i;
	for (i = 0; i < 20; i++) {
		get_rand_str(buf, rand() % 31);
		key = rand() % 32;
		insert(&root, buf, strlen(buf) + 1, &key, sizeof(key), cmp);
	}
	return root;
}

void* destructor(void *p1, void *p2) {
	free(p1);
	free(p2);
	return NULL;
}

tree* search_and_delete(tree *root) { //search and delete example here
	int i;
	for (i = 0; i < 11; i++) {
		char* result = search(root, &i, cmp);
		if (result == NULL)
			printf("Value with key %d not founded\n", i);
		else {
			printf("Found %s - value with key %d. Remove it\n", result, i);
			root = delete(root, &i, cmp, destructor); //delete element with key i
		}
	}
	return root;
}


int main(void) {
	tree *root = getdata();
	print(root);
	putchar('\n');
	root = search_and_delete(root);
	putchar('\n');
	print(root);
	clear(&root, cmp, destructor);
	return EXIT_SUCCESS;
}
